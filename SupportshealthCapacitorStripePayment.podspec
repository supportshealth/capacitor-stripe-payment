
  Pod::Spec.new do |s|
    s.name = 'SupportshealthCapacitorStripePayment'
    s.version = '0.0.2'
    s.summary = 'Make payments using the native Stripe SDKs '
    s.license = 'MIT'
    s.homepage = 'https://bitbucket.org/supportshealth/capacitor-stripe-payment'
    s.author = 'Marcel Canhisares'
    s.source = { :git => 'https://bitbucket.org/supportshealth/capacitor-stripe-payment', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '11.0'
    s.dependency 'Capacitor'
    s.dependency 'Stripe', '18.4.0'
  end