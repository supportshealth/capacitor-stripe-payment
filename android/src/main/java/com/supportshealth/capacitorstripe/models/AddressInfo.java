package com.supportshealth.capacitorstripe.models;

public class AddressInfo {
    private String name;
    private String line1;
    private String line2;
    private String city;
    private String postalCode;
    private String country;
    private String phone;
    private String email;

    public AddressInfo(String name, String line1, String line2, String city, String postalCode, String country, String phone, String email) {
        this.name = name;
        this.line1 = line1;
        this.line2 = line2;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
        this.phone = phone;
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getLine1() {
        return line1;
    }

    public String getLine2() {
        return line2;
    }

    public String getCity() {
        return city;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCountry() {
        return country;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }
}