package com.supportshealth.capacitorstripe.models;

public class CCInfo {
    private String name;
    private String number;
    private Integer expMonth;
    private Integer expYear;
    private String CVC;

    public CCInfo(String name, String number, Integer expMonth, Integer expYear, String CVC) {
        this.name = name;
        this.number = number;
        this.expMonth = expMonth;
        this.expYear = expYear;
        this.CVC = CVC;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public Integer getExpMonth() {
        return expMonth;
    }

    public Integer getExpYear() {
        return expYear;
    }

    public String getCVC() {
        return CVC;
    }
}

