package com.supportshealth.capacitorstripe;

import android.content.Context;
import android.support.annotation.NonNull;

import com.getcapacitor.PluginCall;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Card.Builder;
import com.stripe.android.model.Token;
import com.supportshealth.capacitorstripe.listeners.StripeManagerListener;
import com.supportshealth.capacitorstripe.models.AddressInfo;
import com.supportshealth.capacitorstripe.models.CCInfo;

public class StripeManager {

    private Stripe stripe;
    private StripeManagerListener listener;

    public StripeManager(Context context,  String publishableKey, StripeManagerListener listener) {
        this.stripe = new Stripe(context, publishableKey);
        this.listener = listener;
    }

    public void createToken(CCInfo ccInfo, AddressInfo addressinfo, final PluginCall pluginCall) {

        if (ccInfo == null) {
            listener.onTokenError(new Exception("No credit card information provided"), pluginCall);
            return;
        }

        Builder cardBuilder = new Builder(ccInfo.getNumber(),
                ccInfo.getExpMonth(),
                ccInfo.getExpYear(),
                ccInfo.getCVC());

        cardBuilder.name(ccInfo.getName());

        if (addressinfo != null) {
            cardBuilder.addressLine1(addressinfo.getLine1());
            cardBuilder.addressLine2(addressinfo.getLine2());
            cardBuilder.addressCity(addressinfo.getCity());
            cardBuilder.addressZip(addressinfo.getPostalCode());
            cardBuilder.country(addressinfo.getCountry());
        }

        Card card = cardBuilder.build();


        stripe.createToken(card, new TokenCallback() {
            @Override
            public void onSuccess(@NonNull Token result) {
                listener.onTokenSuccess(result.getId(), pluginCall);
            }

            @Override
            public void onError(@NonNull Exception e) {
                listener.onTokenError(e, pluginCall);
            }
        });
    }

}
