package com.supportshealth.capacitorstripe;

import com.getcapacitor.JSObject;
import com.getcapacitor.NativePlugin;
import com.getcapacitor.Plugin;
import com.getcapacitor.PluginCall;
import com.getcapacitor.PluginMethod;
import com.supportshealth.capacitorstripe.listeners.StripeManagerListener;
import com.supportshealth.capacitorstripe.models.AddressInfo;
import com.supportshealth.capacitorstripe.models.CCInfo;

@NativePlugin()
public class CapacitorStripe extends Plugin implements StripeManagerListener {

    @PluginMethod()
    public void getToken(PluginCall call) {

        String publishableKey = call.getString("publishableKey");
        String ccNumber = call.getString("ccNumber");
        Integer expMonth = call.getInt("expMonth");
        Integer expYear = call.getInt("expYear");
        String cvc = call.getString("cvc");
        String ccName = call.getString("ccName");
        String addressName = call.getString("addressName");
        String addressLine1= call.getString("addressLine1");
        String addressLine2 = call.getString("addressLine2");
        String addressCity = call.getString("addressCity");
        String addressPostalCode = call.getString("addressPostalCode");
        String addressCountry= call.getString("addressCountry");
        String addressPhone = call.getString("addressPhone");
        String addressEmail = call.getString("addressEmail");


        CCInfo ccInfo = new CCInfo(ccName, ccNumber, expMonth, expYear, cvc);
        AddressInfo addressInfo = new AddressInfo(addressName, addressLine1, addressLine2, addressCity,
                addressPostalCode, addressCountry, addressPhone, addressEmail);

        StripeManager manager = new StripeManager(getContext(), publishableKey, this);

        manager.createToken(ccInfo, addressInfo, call);
    }

    @Override
    public void onTokenSuccess(String token, PluginCall pluginCall) {
        JSObject ret = new JSObject();
        ret.put("token", token);
        pluginCall.resolve(ret);
    }

    @Override
    public void onTokenError(Exception e, PluginCall pluginCall) {
        pluginCall.reject(e.getLocalizedMessage(), e);
    }
}
