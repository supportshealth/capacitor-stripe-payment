package com.supportshealth.capacitorstripe.listeners;

import com.getcapacitor.PluginCall;

public interface StripeManagerListener {
    void onTokenSuccess(String token, PluginCall pluginCall);
    void onTokenError(Exception e, PluginCall pluginCall);
}