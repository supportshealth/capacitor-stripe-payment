declare global {
  interface PluginRegistry {
    CapacitorStripe: CapacitorStripePlugin;
  }
}

export interface CapacitorStripePlugin {
  getToken(options: {
    publishableKey: string, ccNumber: string, expMonth: number, expYear: number, cvc: string, ccName?: string,
    addressName?: string, addressLine1?: string, addressLine2?: string, addressCity?: string,
    addressPostalCode?: string, addressCountry?: string, addressPhone?: string, addressEmail?: string
  }): Promise<{ token: string }>;
}
