import Foundation
import Capacitor

/**
 * Please read the Capacitor iOS Plugin Development Guide
 * here: https://capacitor.ionicframework.com/docs/plugins/ios
 */
@objc(CapacitorStripe)
public class CapacitorStripe: CAPPlugin {
    
    @objc func getToken(_ call: CAPPluginCall) {
        
        let publishableKey = call.getString("publishableKey") ?? ""
        let ccNumber = call.getString("ccNumber") ?? ""
        let expMonth = call.getInt("expMonth") ?? 0
        let expYear = call.getInt("expYear") ?? 0
        let cvc = call.getString("cvc") ?? ""
        let ccName = call.getString("ccName") ?? ""
        let addressName = call.getString("addressName")
        let addressLine1 = call.getString("addressLine1")
        let addressLine2 = call.getString("addressLine2")
        let addressCity = call.getString("addressCity")
        let addressPostalCode = call.getString("addressPostalCode")
        let addressCountry = call.getString("addressCountry")
        let addressPhone = call.getString("addressPhone")
        let addressEmail = call.getString("addressEmail")
        
        let stripeManager = StripeManager(publishableKey: publishableKey)
        
        stripeManager.createToken(ccInfo: CCInfo(name: ccName, number: ccNumber, expMonth: expMonth, expYear: expYear, cvc: cvc), addressInfo: AddressInfo(name: addressName, line1: addressLine1, line2: addressLine2, city: addressCity, postalCode: addressPostalCode, country: addressCountry, phone: addressPhone, email: addressEmail)) { (token, err) in
            
            guard let token = token, err == nil else {
                call.reject(err!.localizedDescription, err!, err!.userInfo)
                return
            }
            
            call.resolve([
                "token" : token
            ])
        }
    }
}
