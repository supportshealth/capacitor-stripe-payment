//
//  StripeManager.swift
//  Plugin
//
//  Created by Marcel Canhisares on 2019-07-31.
//  Copyright © 2019 Max Lynch. All rights reserved.
//

import Foundation
import Stripe


class StripeManager {
    
    var stripeApiClient: STPAPIClient
    
    init(publishableKey: String) {
        self.stripeApiClient = STPAPIClient.init(publishableKey: publishableKey)
    }
    
    func createToken(ccInfo: CCInfo, addressInfo: AddressInfo?, completion: @escaping (_ token: String?, _ error: NSError? ) -> ()) {
        let cardParams = STPCardParams()
        cardParams.number = ccInfo.number
        cardParams.expMonth = UInt(ccInfo.expMonth)
        cardParams.expYear = UInt(ccInfo.expYear)
        cardParams.cvc = ccInfo.cvc
        cardParams.name = ccInfo.name
        
        let address = STPAddress()
        address.name = addressInfo?.name
        address.line1 = addressInfo?.line1
        address.line2 = addressInfo?.line2
        address.city = addressInfo?.city
        address.postalCode = addressInfo?.postalCode
        address.country = addressInfo?.country
        address.phone = addressInfo?.phone
        address.email = addressInfo?.email
        
        cardParams.address = address
        
        stripeApiClient.createToken(withCard: cardParams, completion: { (token, error) in
            completion(token?.tokenId, error != nil ? error! as NSError : nil)
        })
    }
    
    
}

struct CCInfo {
    let name: String
    let number: String
    let expMonth: Int
    let expYear: Int
    let cvc: String
    
    init(name: String,
         number: String,
         expMonth: Int,
         expYear: Int,
         cvc: String) {
        self.name = name
        self.number = number
        self.expMonth = expMonth
        self.expYear = expYear
        self.cvc = cvc
    }
}


struct AddressInfo {
    let name: String?
    let line1: String?
    let line2: String?
    let city: String?
    let postalCode: String?
    let country: String?
    let phone: String?
    let email: String?
    
    init(name: String?,
         line1: String?,
         line2: String?,
         city: String?,
         postalCode: String?,
         country: String?,
         phone: String?,
         email: String?) {
        self.name = name
        self.line1 = line1
        self.line2 = line2
        self.city = city
        self.postalCode = postalCode
        self.country = country
        self.phone = phone
        self.email = email
    }
    
}
